package me.rochblondiaux.duke;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.environment.CreatureMapObjectLoader;
import de.gurkenlabs.litiengine.gui.GuiProperties;
import de.gurkenlabs.litiengine.resources.Resources;
import me.rochblondiaux.duke.entity.pig.Pig;
import me.rochblondiaux.duke.screens.InGameScreen;
import me.rochblondiaux.duke.screens.MenuScreen;
import me.rochblondiaux.duke.screens.OptionScreen;
import me.rochblondiaux.duke.ui.buttons.ButtonStyles;
import me.rochblondiaux.duke.ui.popup.PopUpStyles;

import java.awt.*;
import java.util.Locale;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class Duke {

    /* Instance */
    private static Duke instance;

    /* Resources */
    private static final ButtonStyles btnStyles = new ButtonStyles();
    private static final PopUpStyles popUpStyles = new PopUpStyles();

    public static final Font GUI_FONT = Resources.fonts().get("fonts/MinimalPixel.ttf", 48f);
    public static final Font GUI_FONT_SMALL = GUI_FONT.deriveFont(30f);
    public static float HUD_SCALE = 2.0f;

    public Duke(String[] args) {
        /* Instance */
        Duke.instance = this;

        /* Locale */
        Locale.setDefault(new Locale("en", "US"));

        /* Information */
        Game.setInfo("info.xml");

        /* Initialization */
        Game.init(args);
        init();

        /* Start */
        Game.start();
    }

    private void init() {
        /* Icon */
        Game.window().setTitle("Duke");
        Game.window().setIcon(Resources.images().get("icon.png"));

        //Resources.load("maps/level0.litidata");
        Game.graphics().setBaseRenderScale(4f);
        Resources.load("maps/level0.litidata");
        // init default UI settings
        GuiProperties.setDefaultFont(GUI_FONT);


        // Creatures
        CreatureMapObjectLoader.registerCustomCreatureType(Pig.class);

        // UI
        btnStyles.add("large", btnStyles.get("ui/buttons/ui-button-large.png"));
        btnStyles.add("small", btnStyles.get("ui/buttons/ui-button-small.png"));
        btnStyles.add("square", btnStyles.get("ui/buttons/ui-button-square.png"));
        popUpStyles.add("large", popUpStyles.get("ui/popup/large.png"));

        // Screens
        Game.screens().add(new MenuScreen());
        Game.screens().add(new OptionScreen());
        Game.screens().add(new InGameScreen());
    }

    /* Instance */
    public static Duke get() {
        return instance;
    }

    /* Resources */
    public static ButtonStyles buttonStyles() {
        return btnStyles;
    }

    public static PopUpStyles popupStyles() {
        return popUpStyles;
    }
}
