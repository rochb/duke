package me.rochblondiaux.duke;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class Launcher {

    public static void main(String[] args) {
        new Duke(args);
    }
}
