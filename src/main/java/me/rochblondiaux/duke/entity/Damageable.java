package me.rochblondiaux.duke.entity;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public interface Damageable {

    double getMaxHealth();

    void setMaxHealth(double maxHealth);

    double getHealth();

    void setHealth(double health);

    default void damage(double damage) {
        setHealth(getHealth() - damage);
    }

    default boolean isAlive() {
        return getHealth() > 0;
    }
}
