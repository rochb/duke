package me.rochblondiaux.duke.entity.ability;

import de.gurkenlabs.litiengine.abilities.Ability;
import de.gurkenlabs.litiengine.abilities.AbilityInfo;
import de.gurkenlabs.litiengine.entities.EntityPivotType;
import lombok.NonNull;
import me.rochblondiaux.duke.entity.ability.effects.HitEffect;
import me.rochblondiaux.duke.entity.animation.LivingEntityAnimationType;
import me.rochblondiaux.duke.entity.living.LivingEntity;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@AbilityInfo(cooldown = 150, range = 50, impact = 10, impactAngle = 180, value = 1, duration = 400, multiTarget = true, origin = EntityPivotType.DIMENSION_CENTER)
public class AttackAbility extends Ability {

    private final HitEffect hitEffect;

    public AttackAbility(@NonNull LivingEntity executor) {
        super(executor);
        this.hitEffect = new HitEffect(this);
        this.addEffect(hitEffect);

        this.onCast(abilityExecution -> {
            executor.animations().play(LivingEntityAnimationType.ATTACK.getName(executor));
            // TO DAMAGE TO ENTITIES
        });
    }
}
