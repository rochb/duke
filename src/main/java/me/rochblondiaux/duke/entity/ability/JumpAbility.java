package me.rochblondiaux.duke.entity.ability;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.abilities.Ability;
import de.gurkenlabs.litiengine.abilities.AbilityInfo;
import de.gurkenlabs.litiengine.entities.EntityPivotType;
import de.gurkenlabs.litiengine.physics.Collision;
import lombok.NonNull;
import me.rochblondiaux.duke.entity.ability.effects.JumpEffect;
import me.rochblondiaux.duke.entity.animation.LivingEntityAnimationType;
import me.rochblondiaux.duke.entity.living.LivingEntity;

import java.awt.geom.Rectangle2D;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@AbilityInfo(cooldown = 500, origin = EntityPivotType.COLLISIONBOX_CENTER, duration = 300, value = 300)
public class JumpAbility extends Ability {

    public static final int MAX_ADDITIONAL_JUMPS = 1;
    private int consecutiveJumps;
    private long landing = 0;
    private boolean ground = true;
    private double lastY = 0;

    public JumpAbility(@NonNull LivingEntity executor) {
        super(executor);
        this.addEffect(new JumpEffect(this));
    }

    public void call() {
        if (this.consecutiveJumps >= MAX_ADDITIONAL_JUMPS || !canCast())
            return;

        cast();
        this.consecutiveJumps++;
    }

    public void update() {
        LivingEntity entity = (LivingEntity) getExecutor();

        // Animation
        if (lastY != 0 && lastY != entity.getY()) {
            if (lastY > entity.getY())
                entity.animations().play(LivingEntityAnimationType.JUMP.getName(entity));
            else if (lastY < entity.getY())
                entity.animations().play(LivingEntityAnimationType.FALL.getName(entity));
        }

        lastY = entity.getY();
        if (lastY == entity.getY() && isLanding())
            entity.animations().play(LivingEntityAnimationType.LAND.getName(entity));

        if (!isTouchingGround()) {
            this.ground = false;
            return;
        }

        if (!ground)
            this.landing = System.currentTimeMillis();


        // Reset
        this.consecutiveJumps = 0;
        this.ground = true;
    }

    public boolean isTouchingGround() {
        Rectangle2D groundCheck = new Rectangle2D.Double(getExecutor().getCollisionBox().getX(),
                getExecutor().getCollisionBox().getY(),
                getExecutor().getCollisionBoxWidth(),
                getExecutor().getCollisionBoxHeight() + 1);

        if (groundCheck.getMaxY() > Game.physics().getBounds().getMaxY())
            return true;
        return Game.physics().collides(groundCheck, Collision.STATIC);
    }

    public boolean isLanding() {
        if (landing == 0)
            return false;
        return System.currentTimeMillis() - landing <= 100;
    }
}
