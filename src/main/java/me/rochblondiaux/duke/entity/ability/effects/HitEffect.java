package me.rochblondiaux.duke.entity.ability.effects;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.abilities.Ability;
import de.gurkenlabs.litiengine.abilities.effects.Effect;
import de.gurkenlabs.litiengine.abilities.effects.EffectTarget;
import de.gurkenlabs.litiengine.entities.Creature;
import de.gurkenlabs.litiengine.entities.ICombatEntity;
import de.gurkenlabs.litiengine.physics.Collision;
import de.gurkenlabs.litiengine.physics.Force;
import de.gurkenlabs.litiengine.physics.GravityForce;
import lombok.NonNull;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HitEffect extends Effect {

    public HitEffect(@NonNull Ability ability) {
        super(ability, EffectTarget.ENEMY);
    }

    @Override
    public void apply(final ICombatEntity affectedEntity) {
        if (affectedEntity.isIndestructible())
            return;
        super.apply(affectedEntity);
        double damage = getAbility().getAttributes().value().get();
        affectedEntity.hit((int) damage, this.getAbility());
    }

    @Override
    protected Collection<ICombatEntity> getEntitiesInImpactArea(final Shape impactArea) {
        final List<ICombatEntity> entities = new ArrayList<>();
        double range = getAbility().getAttributes().range().get();

        for (final ICombatEntity entity : Game.world()
                .environment()
                .findCombatEntities(impactArea, x -> x instanceof Creature)) {
            final Point2D collCenterExecutor = new Point2D.Double(
                    this.getAbility().getExecutor().getCollisionBox().getCenterX(),
                    this.getAbility().getExecutor().getCollisionBox().getCenterY());
            final Point2D collCenterEntity = new Point2D.Double(entity.getCollisionBox().getCenterX(),
                    entity.getCollisionBox().getCenterY());
            final Line2D rayCast = new Line2D.Double(collCenterExecutor, collCenterEntity);

            boolean collision = false;
            for (final Rectangle2D collisionBox : Game.physics().getCollisionBoxes(Collision.STATIC)) {
                if (collisionBox.intersectsLine(rayCast)) {
                    collision = true;
                    break;
                }
            }

            if (!collision)
                entities.add(entity);
        }
        return entities;
    }
}