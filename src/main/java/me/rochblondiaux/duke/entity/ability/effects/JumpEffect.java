package me.rochblondiaux.duke.entity.ability.effects;

import de.gurkenlabs.litiengine.Direction;
import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.abilities.Ability;
import de.gurkenlabs.litiengine.abilities.effects.EffectApplication;
import de.gurkenlabs.litiengine.abilities.effects.EffectTarget;
import de.gurkenlabs.litiengine.abilities.effects.ForceEffect;
import de.gurkenlabs.litiengine.entities.IMobileEntity;
import de.gurkenlabs.litiengine.physics.Force;
import de.gurkenlabs.litiengine.physics.GravityForce;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class JumpEffect extends ForceEffect {

    public JumpEffect(Ability ability) {
        super(ability, ability.getAttributes().value().get(), EffectTarget.EXECUTINGENTITY);
    }

    @Override
    protected Force applyForce(IMobileEntity affectedEntity) {
        GravityForce force = new GravityForce(affectedEntity, this.getStrength(), Direction.UP);
        affectedEntity.movement().apply(force);
        return force;
    }

    @Override
    protected boolean hasEnded(final EffectApplication appliance) {
        return super.hasEnded(appliance) || this.isTouchingCeiling();
    }

    /**
     * Make sure that the jump is cancelled when the entity touches a static collision box above it.
     *
     * @return True if the entity touches a static collision box above it.
     */
    private boolean isTouchingCeiling() {
        return Game.world()
                .environment()
                .getCollisionBoxes()
                .stream()
                .filter(x -> x.getBoundingBox().intersects(this.getAbility().getExecutor().getBoundingBox()))
                .findFirst()
                .filter(box -> box.getCollisionBox().getMaxY() < this.getAbility().getExecutor().getCollisionBox().getMinY())
                .isPresent();
    }
}