package me.rochblondiaux.duke.entity.ability.effects;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.abilities.Ability;
import de.gurkenlabs.litiengine.abilities.effects.Effect;
import lombok.NonNull;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ScreenShakeEffect extends Effect {

    private final int duration;

    private final double intensity;

    public ScreenShakeEffect(@NonNull Ability ability, @NonNull  double intensity, @NonNull int duration) {
        super(ability);
        this.duration = duration;
        this.intensity = intensity;
    }

    @Override
    public void apply(final Shape impactArea) {
        super.apply(impactArea);
        Game.world().camera().shake(this.intensity, 30, this.duration);
    }
}
