package me.rochblondiaux.duke.entity.animation;

import de.gurkenlabs.litiengine.graphics.Spritesheet;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.resources.Resources;
import lombok.NonNull;
import me.rochblondiaux.duke.entity.living.LivingEntity;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum LivingEntityAnimationType {
    FALL,
    LAND,
    JUMP,
    ATTACK,
    DAMAGE,
    DEAD;

    public Spritesheet getSpriteSheet(@NonNull String prefix) {
        Spritesheet spritesheet = Resources.spritesheets().get(prefix + "-" + name().toLowerCase() + "-right");
        if (Objects.isNull(spritesheet))
            spritesheet = Resources.spritesheets().get(prefix + "-" + name().toLowerCase() + "-left");
        return spritesheet;
    }

    public Animation getAnimation(@NonNull String prefix, @NonNull String name, boolean loop, int... duration) {
        Spritesheet spritesheet = getSpriteSheet(prefix);
        if (Objects.isNull(spritesheet))
            return null;
        return new Animation(name, spritesheet, loop, duration);
    }

    public Animation getAnimation(@NonNull String prefix, boolean loop, boolean randomizeStart, int... duration) {
        Spritesheet spritesheet = getSpriteSheet(prefix);
        if (Objects.isNull(spritesheet))
            return null;
        return new Animation(spritesheet, loop, randomizeStart, duration);
    }

    public Animation getAnimation(@NonNull String prefix, boolean loop, int... duration) {
        Spritesheet spritesheet = getSpriteSheet(prefix);
        if (Objects.isNull(spritesheet))
            return null;
        return new Animation(spritesheet, loop, duration);
    }


    public String getName(@NonNull LivingEntity entity) {
        String baseName = entity.getPrefix() + "-" + name().toLowerCase() + "-";
        if (entity.animations().hasAnimation(baseName + "right"))
            return baseName + "right";
        return baseName + "left";
    }
}
