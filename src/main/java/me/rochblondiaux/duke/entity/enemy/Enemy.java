package me.rochblondiaux.duke.entity.enemy;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.CombatEntityListener;
import de.gurkenlabs.litiengine.entities.EntityHitEvent;
import de.gurkenlabs.litiengine.entities.ICombatEntity;
import de.gurkenlabs.litiengine.environment.Environment;
import de.gurkenlabs.litiengine.graphics.RenderType;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.sound.Sound;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import me.rochblondiaux.duke.entity.living.LivingEntity;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Setter
@Getter
public abstract class Enemy extends LivingEntity {

    private static final Sound ENEMY_DAMAGE_SOUND = Resources.sounds().get("audio/enemies/enemy-damage.ogg");
    private static final Sound ENEMY_DIES_SOUND = Resources.sounds().get("audio/enemies/enemy-dies.ogg");

    private EnemyState state;
    private final double maxRange;

    public Enemy(@NonNull String name, double maxHealth, double maxRange) {
        super(name, maxHealth);
        this.maxRange = maxRange;
        this.state = EnemyState.IDLING;

       // addController(new EnemyController(this));

        addCombatEntityListener(new CombatEntityListener() {
            @Override
            public void death(ICombatEntity iCombatEntity) {
                Game.audio().playSound(ENEMY_DIES_SOUND).setVolume(Game.config().sound().getSoundVolume() / 2);
            }

            @Override
            public void hit(EntityHitEvent entityHitEvent) {
                Game.audio().playSound(ENEMY_DAMAGE_SOUND);
            }

            @Override
            public void resurrect(ICombatEntity iCombatEntity) {

            }
        });
    }

    @Override
    public void loaded(Environment environment) {
        super.loaded(environment);

        environment.add(new EnemyHealthBar(this), RenderType.OVERLAY);
    }
}
