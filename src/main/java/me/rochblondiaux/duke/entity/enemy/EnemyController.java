package me.rochblondiaux.duke.entity.enemy;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.behavior.AStarPathFinder;
import de.gurkenlabs.litiengine.entities.behavior.EntityNavigator;
import de.gurkenlabs.litiengine.entities.behavior.IBehaviorController;
import lombok.Data;
import me.rochblondiaux.duke.entity.player.Player;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class EnemyController implements IBehaviorController {

    private final Enemy entity;
    private final EntityNavigator navigator;

    public EnemyController(Enemy entity) {
        this.entity = entity;
        this.navigator = new EntityNavigator(entity, new AStarPathFinder(Game.world().environment().getMap()));
        this.navigator.setAcceptableError(1);
    }

    public void updateState() {
        if (Player.get().getCenter().distance(entity.getCenter()) <= entity.getAttackAbility().getAttributes().range().get() && !entity.getState().equals(EnemyState.ATTACKING))
            entity.setState(EnemyState.ATTACKING);
        else if (Player.get().getCenter().distance(entity.getCenter()) <= entity.getMaxRange() && !entity.getState().equals(EnemyState.CHASING)) {
            entity.setState(EnemyState.CHASING);
            navigator.navigate(Player.get().getHitBox().getBounds().getLocation());
        } else
            entity.setState(EnemyState.IDLING);
    }

    @Override
    public void update() {
        updateState();
        switch (entity.getState()) {
            case CHASING:
                navigator.update();
                break;
            case ATTACKING:
                if (entity.getAttackAbility().canCast())
                    entity.getAttackAbility().cast();
                break;
            default:
            case IDLING:
                break;
        }
    }
}
