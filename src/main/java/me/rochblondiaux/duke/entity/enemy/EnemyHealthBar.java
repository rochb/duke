package me.rochblondiaux.duke.entity.enemy;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.graphics.IRenderable;
import lombok.Data;

import java.awt.*;
import java.awt.geom.RoundRectangle2D;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class EnemyHealthBar implements IRenderable {

    private final Enemy enemy;

    @Override
    public void render(Graphics2D g) {
        if (enemy.isDead())
            return;
        final double width = 16;
        final double height = 2;
        double x = enemy.getX() - (width - enemy.getWidth()) / 2.0;
        double y = enemy.getY() - (height * 0.75);
        RoundRectangle2D rect = new RoundRectangle2D.Double(x, y, width, height, 1.5, 1.5);

        double health = enemy.getHealth();
        double maxHealth = enemy.getMaxHealth();

        final double currentWidth = width * (health / maxHealth);
        RoundRectangle2D actualRect = new RoundRectangle2D.Double(x, y, currentWidth, height, 1.5, 1.5);

        g.setColor(new Color(40, 42, 43, 200));
        Game.graphics().renderShape(g, rect);

        if (health > maxHealth / 2)
            g.setColor(new Color(46, 204, 113));
        else if (health < maxHealth / 2 && health > maxHealth / 3)
            g.setColor(new Color(230, 126, 34));
        else if (health < maxHealth / 3)
            g.setColor(new Color(231, 76, 60));
        Game.graphics().renderShape(g, actualRect);
    }
}
