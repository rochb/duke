package me.rochblondiaux.duke.entity.enemy;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum EnemyState {
    IDLING,
    CHASING,
    ATTACKING,
}
