package me.rochblondiaux.duke.entity.living;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.IUpdateable;
import de.gurkenlabs.litiengine.entities.*;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.graphics.animation.IAnimationController;
import de.gurkenlabs.litiengine.graphics.animation.IEntityAnimationController;
import de.gurkenlabs.litiengine.physics.GravityForce;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import me.rochblondiaux.duke.entity.Damageable;
import me.rochblondiaux.duke.entity.ability.AttackAbility;
import me.rochblondiaux.duke.entity.ability.JumpAbility;
import me.rochblondiaux.duke.entity.animation.LivingEntityAnimationType;
import me.rochblondiaux.duke.entity.player.Player;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Setter
@Getter
public abstract class LivingEntity extends Creature implements Damageable, IUpdateable, ICombatEntity {

    /* Health */
    private double maxHealth;
    private double health;

    /* Abilities */
    private final JumpAbility jumpAbility;
    private final AttackAbility attackAbility;

    /* Properties */
    private long lastHit = 0;

    public LivingEntity(@NonNull String name, double maxHealth) {
        super(name);
        setIndestructible(false);

        // Health
        this.maxHealth = maxHealth;
        this.health = maxHealth;

        // Abilities
        this.jumpAbility = new JumpAbility(this);
        this.attackAbility = new AttackAbility(this);

        // Combat
        addCombatEntityListener(new CombatEntityListener() {
            @Override
            public void death(ICombatEntity e) {
                setHealth(0);
            }

            @Override
            public void hit(EntityHitEvent e) {
                LivingEntity damaged = (LivingEntity) e.getHitEntity();
                damaged.damage(e.getDamage() / 2d);

                // Knockback
                GravityForce force = new GravityForce(damaged, 150, ((Creature) e.getExecutor()).getFacingDirection());
                damaged.movement().apply(force);

                // Knockback end & damage animation
                Game.loop().perform(100, () -> {
                    damaged.animations().play(LivingEntityAnimationType.DAMAGE.getName(damaged));
                    force.end();
                });
            }

            @Override
            public void resurrect(ICombatEntity e) {
                setHealth(maxHealth);
            }
        });
    }

    @Override
    public void update() {
        // Abilities
        jumpAbility.update();

        // Hit
        if (lastHit != 0 && System.currentTimeMillis() - lastHit > 150)
            lastHit = 0;

        // Health
        if (getHealth() <= 0) {
            if (!(this instanceof Player))
                animations().play(LivingEntityAnimationType.DEAD.getName(this));
            Game.loop().perform(350, () -> Game.world().environment().remove(this));
            getHitPoints().setBaseValue(0);
        }
    }

    /* Animations */
    protected void initializeAnimationController() {
        IAnimationController controller = this.animations();

        // Animations
        for (LivingEntityAnimationType a : LivingEntityAnimationType.values()) {
            Animation animation = a.getAnimation(getPrefix(), false);
            if (Objects.nonNull(animation))
                controller.add(animation);
        }
    }

    @Override
    protected IEntityAnimationController<?> createAnimationController() {
        return new LivingEntityAnimationController(this);
    }

    @Override
    protected void updateAnimationController() {
        super.updateAnimationController();
        this.initializeAnimationController();
    }

    /* Abilities */
    @Action(description = "This performs the jump ability for the current entity.")
    public void jump() {
        jumpAbility.call();
    }

    @Action(description = "This performs the default attack ability for the current entity.")
    public void attack() {
        attackAbility.cast();
    }


    /* Getters */
    public boolean isLanding() {
        return jumpAbility.isLanding();
    }

    public boolean isFalling() {
        return !isOnGround();
    }

    public boolean isOnGround() {
        return jumpAbility.isTouchingGround();
    }

    public boolean isJumping() {
        return jumpAbility.isActive();
    }

    public boolean isHit() {
        return lastHit != 0;
    }

    public abstract String getPrefix();

    @Override
    public boolean isDead() {
        return !isAlive();
    }
}
