package me.rochblondiaux.duke.entity.living;

import de.gurkenlabs.litiengine.Direction;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.graphics.animation.CreatureAnimationController;
import de.gurkenlabs.litiengine.util.Imaging;
import lombok.NonNull;

import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class LivingEntityAnimationController extends CreatureAnimationController<LivingEntity> {

    public LivingEntityAnimationController(@NonNull LivingEntity creature) {
        super(creature, false);
    }

    @Override
    public BufferedImage getCurrentImage() {
        BufferedImage spriteImage = super.getCurrentImage();
        if (spriteImage == null)
            return null;
        Animation animation = getEntity().animations().getCurrent();
        if (Objects.nonNull(animation)) {
            if ((animation.getName().contains("left")
                    && getEntity().getFacingDirection().equals(Direction.RIGHT))
                    || (animation.getName().contains("right")
                    && getEntity().getFacingDirection().equals(Direction.LEFT)))
                return Imaging.horizontalFlip(spriteImage);
        }
        return spriteImage;
    }
}
