package me.rochblondiaux.duke.entity.pathfinder;

import de.gurkenlabs.litiengine.entities.IMobileEntity;
import de.gurkenlabs.litiengine.entities.behavior.Path;
import de.gurkenlabs.litiengine.entities.behavior.PathFinder;

import java.awt.geom.Point2D;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class EnemyPathFinder extends PathFinder {

    @Override
    public Path findPath(IMobileEntity iMobileEntity, Point2D point2D) {
        return null;
    }
}
