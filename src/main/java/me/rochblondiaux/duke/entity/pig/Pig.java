package me.rochblondiaux.duke.entity.pig;

import de.gurkenlabs.litiengine.Align;
import de.gurkenlabs.litiengine.Valign;
import de.gurkenlabs.litiengine.entities.CollisionInfo;
import de.gurkenlabs.litiengine.entities.CombatInfo;
import de.gurkenlabs.litiengine.entities.EntityInfo;
import de.gurkenlabs.litiengine.entities.MovementInfo;
import me.rochblondiaux.duke.entity.enemy.Enemy;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@EntityInfo(width = 19, height = 19)
@MovementInfo(velocity = 40)
@CollisionInfo(collisionBoxWidth = 19, collisionBoxHeight = 19, collision = true, align = Align.CENTER, valign = Valign.MIDDLE)
@CombatInfo(hitpoints = 1000, team = 2, isIndestructible = true)
public class Pig extends Enemy {

    public Pig() {
        super("pig/pig", 10, 500);
    }

    @Override
    public String getPrefix() {
        return "pig";
    }
}
