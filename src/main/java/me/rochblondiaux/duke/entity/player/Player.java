package me.rochblondiaux.duke.entity.player;

import de.gurkenlabs.litiengine.Align;
import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.Valign;
import de.gurkenlabs.litiengine.entities.*;
import de.gurkenlabs.litiengine.graphics.animation.Animation;
import de.gurkenlabs.litiengine.graphics.animation.IAnimationController;
import de.gurkenlabs.litiengine.physics.IMovementController;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.sound.SFXPlayback;
import de.gurkenlabs.litiengine.sound.Sound;
import me.rochblondiaux.duke.entity.living.LivingEntity;
import me.rochblondiaux.duke.input.DukeMovementController;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@EntityInfo(width = 78, height = 58)
@MovementInfo(velocity = 70)
@CollisionInfo(collisionBoxWidth = 37, collisionBoxHeight = 27, collision = true, align = Align.CENTER, valign = Valign.MIDDLE)
@CombatInfo(hitpoints = 1000, team = 1)
public class Player extends LivingEntity {

    private static Player instance;
    private static final Sound JUMP_SOUND = Resources.sounds().get("audio/player/jump.wav");
    private static final Sound WALK_SOUND = Resources.sounds().get("audio/player/walk.wav");

    private final Map<String, SFXPlayback> sounds;

    private Player() {
        super("player/player", 20);
        sounds = new HashMap<>();

        addCombatEntityListener(new CombatEntityListener() {
            @Override
            public void death(ICombatEntity iCombatEntity) {

            }

            @Override
            public void hit(EntityHitEvent e) {
                Game.world().camera().shake(0.5, 30, 250);
            }

            @Override
            public void resurrect(ICombatEntity iCombatEntity) {

            }
        });
    }

    @Override
    public void update() {
        super.update();

        if (isJumping() && (!sounds.containsKey("jump") || !sounds.get("jump").isPlaying())) {
            SFXPlayback sound = Game.audio().playSound(JUMP_SOUND);
            sound.setVolume(Game.config().sound().getSoundVolume() / 2);
            sounds.put("jump", sound);
        }
    }

    @Override
    protected IMovementController createMovementController() {
        DukeMovementController controller = new DukeMovementController(this);
        controller.setJumpKeys(KeyEvent.VK_UP, KeyEvent.VK_SPACE);
        controller.setDownKeys(KeyEvent.VK_DOWN, KeyEvent.VK_S);
        controller.setLeftKeys(KeyEvent.VK_Q, KeyEvent.VK_LEFT);
        controller.setRightKeys(KeyEvent.VK_D, KeyEvent.VK_RIGHT);
        controller.addAttackKey(KeyEvent.VK_E);
        return controller;
    }

    @Override
    protected void initializeAnimationController() {
        IAnimationController controller = this.animations();
        controller.add(new Animation(Resources.spritesheets().get("player-doorout-right"), false));
        controller.add(new Animation(Resources.spritesheets().get("player-doorin-right"), false));
        controller.add(new Animation(Resources.spritesheets().get("player-dead-right"), false));
        super.initializeAnimationController();
    }

    @Override
    public String getPrefix() {
        return "player";
    }


    /* Instance */
    public static Player get() {
        if (instance == null)
            instance = new Player();
        return instance;
    }
}