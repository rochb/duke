package me.rochblondiaux.duke.game;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.entities.Spawnpoint;
import de.gurkenlabs.litiengine.graphics.Camera;
import de.gurkenlabs.litiengine.graphics.PositionLockCamera;
import lombok.Data;
import lombok.NonNull;
import me.rochblondiaux.duke.entity.player.Player;
import me.rochblondiaux.duke.screens.InGameScreen;
import me.rochblondiaux.duke.screens.MenuScreen;
import me.rochblondiaux.duke.screens.OptionScreen;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class GameManager {

    private static GameState state = GameState.MENU;

    public static void init() {
        // Init camera
        Camera camera = new PositionLockCamera(Player.get());
        camera.setClampToMap(true);
        Game.world().setCamera(camera);

        // Set gravity
        Game.world().setGravity(120);

        // Add world loaded event listener
        Game.world().onLoaded(e -> {
            Spawnpoint enter = e.getSpawnpoint("spawn");
            if (enter != null)
                enter.spawn(Player.get());
        });

        // Load world
        Game.world().loadEnvironment("level0");
    }

    public static boolean is(@NonNull GameState state) {
        return GameManager.state.equals(state);
    }

    public static void setState(@NonNull GameState state) {
        if (GameManager.state.equals(state))
            return;
        switch (state) {
            case MENU:
                Game.screens().display(MenuScreen.NAME);
                break;
            case OPTION:
                Game.screens().display(OptionScreen.NAME);
                break;
            case IN_GAME:
                if (!GameManager.state.equals(GameState.PAUSED)) {
                    init();
                    Game.screens().display(InGameScreen.NAME);
                } else {
                    InGameScreen screen = (InGameScreen) Game.screens().get(InGameScreen.NAME);
                    screen.getPopUp().setVisible(false);
                }
                break;
            case PAUSED:
                InGameScreen screen = (InGameScreen) Game.screens().get(InGameScreen.NAME);
                screen.getPopUp().setVisible(true);
                break;
            case TUTORIAL:
                break;
        }
        GameManager.state = state;
    }
}
