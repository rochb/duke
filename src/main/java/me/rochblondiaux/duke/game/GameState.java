package me.rochblondiaux.duke.game;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public enum GameState {
    MENU,
    OPTION,
    IN_GAME,
    TUTORIAL,
    PAUSED
}
