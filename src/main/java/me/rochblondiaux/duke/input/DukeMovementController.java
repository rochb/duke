package me.rochblondiaux.duke.input;

import de.gurkenlabs.litiengine.input.Input;
import de.gurkenlabs.litiengine.input.KeyboardEntityController;
import de.gurkenlabs.litiengine.util.ListUtilities;
import me.rochblondiaux.duke.entity.player.Player;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class DukeMovementController extends KeyboardEntityController<Player> {


    public static final String JUMP_ACTION = "jump";
    public static final String ATTACK_ACTION = "attack";

    /**
     * The list of jump keys, represented by their integer values.
     */
    private final List<Integer> jump;
    private final List<Integer> attack;

    /**
     * Instantiates a new platforming movement controller.
     *
     * @param entity the entity
     */
    public DukeMovementController(final Player entity) {
        this(entity, KeyEvent.VK_SPACE, KeyEvent.VK_A);
    }

    /**
     * Instantiates a new platforming movement controller.
     *
     * @param entity the entity
     * @param jump   the jump
     */
    public DukeMovementController(Player entity, final int jump, final int attack) {
        super(entity);
        this.getUpKeys().clear();
        this.getDownKeys().clear();
        this.jump = new ArrayList<>();
        this.attack = new ArrayList<>();
        this.addJumpKey(jump);
        this.addAttackKey(attack);
        Input.keyboard().onKeyPressed(this::handlePressedKey);
    }

    @Override
    public void handlePressedKey(KeyEvent keyCode) {
        super.handlePressedKey(keyCode);
        if (this.jump.contains(keyCode.getKeyCode()))
            this.getEntity().perform(JUMP_ACTION);
        else if (this.attack.contains(keyCode.getKeyCode()))
            this.getEntity().perform(ATTACK_ACTION);
    }

    /**
     * Adds a jump key.
     *
     * @param keyCode the key code for the newly added jump key
     */
    public void addJumpKey(int keyCode) {
        if (this.jump.contains(keyCode))
            return;

        this.jump.add(keyCode);
    }

    public void addAttackKey(int keyCode) {
        if (this.attack.contains(keyCode))
            return;

        this.attack.add(keyCode);
    }

    /**
     * Gets the list of jump key codes in this controller.
     *
     * @return the jump keys
     */
    public List<Integer> getJumpKeys() {
        return this.jump;
    }

    /**
     * Initializes the jump keys with a given array of key codes.
     *
     * @param jump the new jump keys
     */
    public void setJumpKeys(int... jump) {
        this.setUpKeys(ListUtilities.getIntList(jump));
    }
}