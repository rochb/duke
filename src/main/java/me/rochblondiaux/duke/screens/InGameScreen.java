package me.rochblondiaux.duke.screens;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.gui.Appearance;
import de.gurkenlabs.litiengine.gui.screens.GameScreen;
import de.gurkenlabs.litiengine.input.Input;
import lombok.Getter;
import me.rochblondiaux.duke.Duke;
import me.rochblondiaux.duke.game.GameManager;
import me.rochblondiaux.duke.game.GameState;
import me.rochblondiaux.duke.ui.HUD;
import me.rochblondiaux.duke.ui.popup.PausePopUp;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
public class InGameScreen extends GameScreen {

    public static final String NAME = "GAME";
    private final HUD hud;
    private PausePopUp popUp;

    public InGameScreen() {
        super(NAME);
        this.hud = new HUD();
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
        this.hud.render(g);

        // TODO: stop user input & freeze game
    }

    @Override
    public void prepare() {
        super.prepare();
        popUp.setVisible(false);

        Input.keyboard()
                .onKeyTyped(KeyEvent.VK_ESCAPE, e -> {
                    if (GameManager.is(GameState.PAUSED))
                        GameManager.setState(GameState.IN_GAME);
                    else
                        GameManager.setState(GameState.PAUSED);
                });
    }

    @Override
    protected void initializeComponents() {
        super.initializeComponents();

        // Pause
        this.popUp = new PausePopUp(Duke.popupStyles().get("large"), Game.window().getCenter().getX(), Game.window().getCenter().getY(), 10);
        popUp.setLocation(Game.window().getCenter().getX() - (popUp.getWidth() / 2), Game.window().getCenter().getY() - (popUp.getHeight() / 2));
        this.getComponents().add(popUp);
        Appearance appearance;
    }
}
