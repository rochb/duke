package me.rochblondiaux.duke.screens;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.graphics.ImageRenderer;
import de.gurkenlabs.litiengine.graphics.TextRenderer;
import de.gurkenlabs.litiengine.gui.ComponentMouseEvent;
import de.gurkenlabs.litiengine.gui.screens.Screen;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.sound.LoopedTrack;
import de.gurkenlabs.litiengine.sound.Sound;
import lombok.NonNull;
import me.rochblondiaux.duke.Duke;
import me.rochblondiaux.duke.game.GameManager;
import me.rochblondiaux.duke.game.GameState;
import me.rochblondiaux.duke.ui.buttons.Button;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class MenuScreen extends Screen {

    public static final String NAME = "MENU";
    private final BufferedImage logo = Resources.images().get("icon.png");
    public final Sound CLICK_MUSIC = Resources.sounds().get("audio/ui/select.wav");
    public final LoopedTrack MAIN_MUSIC = new LoopedTrack(Resources.sounds().get("audio/music.wav"));

    public MenuScreen() {
        super(NAME);
    }

    @Override
    public void render(Graphics2D g) {
        renderGameLogo(g);

        // render nesty logo
        BufferedImage nestyLogo = Resources.images().get("nesty-logo.png");
        double scale = 0.15;
        final double x = Game.window().getResolution().getWidth() - (nestyLogo.getWidth() * scale) - 10;
        final double y = Game.window().getResolution().getHeight() - (nestyLogo.getHeight() * scale) - 10;
        ImageRenderer.renderScaled(g, nestyLogo, x, y, scale);

        // render info
        String info1 = "by Nesty Service";
        g.setFont(Duke.GUI_FONT_SMALL);
        g.setColor(Color.WHITE);
        double infoX1 = Game.window().getCenter().getX() - g.getFontMetrics().stringWidth(info1) / 2.0;
        double infoY1 = Game.window().getResolution().getHeight() - g.getFontMetrics().getHeight() - 10;
        TextRenderer.render(g, info1, infoX1, infoY1);

        AtomicInteger buttons = new AtomicInteger(0);
        getComponents()
                .stream()
                .filter(guiComponent -> guiComponent instanceof Button)
                .forEach(guiComponent -> {
                    double width = Game.window().getWidth() / 10d;
                    double height = Game.window().getHeight() / 12d;
                    guiComponent.setLocation((Game.window().getWidth() / 2d) - (width / 2.0),
                            Game.window().getHeight() / 2d + (height / 2) + (buttons.getAndIncrement() * Game.window().getHeight() / 10d));
                    guiComponent.setDimension(width, height);
                });

        super.render(g);
    }

    @Override
    public void prepare() {
        super.prepare();
    }

    @Override
    protected void initializeComponents() {
        super.initializeComponents();
        double x = Game.window().getWidth() / 2d;
        double y = Game.window().getHeight() / 2d;
        double space = Game.window().getHeight() / 10d;

        /* Play */
        addButton("Play", x, y, componentMouseEvent -> {
            Game.audio().playMusic(MAIN_MUSIC, true)
                    .setVolume(Game.config().sound().getMusicVolume() / 2);
            Game.window().getRenderComponent().fadeOut(1000);
            Game.loop().perform(1500, () -> {
                GameManager.setState(GameState.IN_GAME);
                Game.window().getRenderComponent().fadeIn(1000);
            });
        });

        /* Options */
        y += space;
        addButton("Options", x, y, componentMouseEvent -> {
            Game.window().getRenderComponent().fadeOut(1000);
            Game.loop().perform(1500, () -> {
                GameManager.setState(GameState.OPTION);
                Game.window().getRenderComponent().fadeIn(1000);
            });
        });

        /* Quit */
        y += space;
        addButton("Quit", x, y, componentMouseEvent -> System.exit(0));
    }

    private void addButton(@NonNull String text, double x, double y, @NonNull Consumer<ComponentMouseEvent> clickAction) {
        double width = Game.window().getWidth() / 10d;
        double height = Game.window().getHeight() / 12d;

        Button button = new Button(Duke.buttonStyles().get("small"), text, x - width / 2.0, y + height / 2.0);
        button.setDimension(width, height);
        button.setFont(Duke.GUI_FONT.deriveFont(75f));
        button.onClicked(componentMouseEvent -> {
            Game.audio().playSound(CLICK_MUSIC);
            getComponents().forEach(guiComponent -> guiComponent.setEnabled(false));
            clickAction.accept(componentMouseEvent);
            Game.loop().perform(1500, () -> getComponents().forEach(guiComponent -> guiComponent.setEnabled(true)));
        });
        this.getComponents().add(button);
    }

    private void renderGameLogo(Graphics2D g) {
        // render game logo
        double scale = 5;
        double x = Game.window().getCenter().getX() - (logo.getWidth() * scale) / 2;
        double y = Game.window().getCenter().getY() - (logo.getHeight() * scale);
        ImageRenderer.renderScaled(g, logo, x, y, scale);
    }


}
