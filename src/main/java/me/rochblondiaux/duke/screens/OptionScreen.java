package me.rochblondiaux.duke.screens;

import de.gurkenlabs.litiengine.gui.screens.Screen;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class OptionScreen extends Screen {

    public static final String NAME = "OPTIONS";

    public OptionScreen() {
        super(NAME);
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
    }

    @Override
    protected void initializeComponents() {
        super.initializeComponents();
    }

    @Override
    public void prepare() {
        super.prepare();
    }
}
