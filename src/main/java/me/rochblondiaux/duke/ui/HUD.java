package me.rochblondiaux.duke.ui;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.graphics.ImageRenderer;
import de.gurkenlabs.litiengine.graphics.Spritesheet;
import de.gurkenlabs.litiengine.graphics.TextRenderer;
import de.gurkenlabs.litiengine.gui.GuiComponent;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.util.Imaging;
import lombok.NonNull;
import me.rochblondiaux.duke.entity.player.Player;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class HUD extends GuiComponent {

    private static HUD instance;
    private static final BufferedImage HEALTH_BAR;
    private static final BufferedImage HEART;
    private static final BufferedImage HALF_HEART;
    private static final BufferedImage EMPTY_HEART;
    private static final Spritesheet HEART_HIT;
    private static final double HEART_SCALE = 2.75d;

    static {
        HEALTH_BAR = Imaging.scale(Resources.images().get("ui/hud/health-bar.png"), 4d);
        HEART = Imaging.scale(Resources.images().get("ui/hud/heart.png"), HEART_SCALE);
        HALF_HEART = Imaging.scale(Resources.images().get("ui/hud/half-heart.png"), HEART_SCALE);
        EMPTY_HEART = Imaging.scale(Resources.images().get("ui/hud/empty-heart.png"), HEART_SCALE);
        HEART_HIT = Resources.spritesheets().get("heart-hit");
    }

    private final Map<Integer, Point2D> hearts = new HashMap<>();

    public HUD() {
        super(0, 0);
        instance = this;
    }

    @Override
    public void render(Graphics2D g) {
        super.render(g);
        if (Objects.isNull(Game.world().environment()))
            return;
        TextRenderer.enableTextAntiAliasing(g);

        // Health Bar
        renderHealthBar(g);
    }

    private Point2D getHeartLocation(double x, double y, int height, int index) {
        for (int i = 0; i < index; i++)
            x += HEART.getWidth() + (height / 25d);
        return new Point((int) x, (int) y);
    }

    private void renderHealthBar(@NonNull Graphics2D g) {
        int height = HEALTH_BAR.getHeight();
        int width = HEALTH_BAR.getWidth();
        double initialX = Game.window().getCenter().getX() - width / 2.0;
        double initialY = Game.window().getResolution().getHeight() - height - 10;

        double x = initialX + ((height / 7d) * 4.40);
        double y = initialY + (height * 0.37);

        if (hearts.size() == 0) {
            for (int i = 0; i < 10; i++)
                hearts.put(i, getHeartLocation(x, y, height, i));
        }

        // Health bar
        ImageRenderer.render(g, HEALTH_BAR, initialX, initialY);

        // Hearts
        Player player = Player.get();
        int maxHearts = (int) (player.getMaxHealth() / 2);
        if (maxHearts > 10)
            maxHearts = 10;
        double health = player.getHealth();
        for (int i = 0; i < maxHearts; i++) {
            if (!this.hearts.containsKey(i))
                continue;
            Point2D position = this.hearts.get(i);
            if (health > 0) {
                if (health == 1)
                    ImageRenderer.render(g, HALF_HEART, position);
                else
                    ImageRenderer.render(g, HEART, position);
            } else
                ImageRenderer.render(g, EMPTY_HEART, position);
            health -= 2;
        }
    }

    public static HUD get() {
        return instance;
    }
}
