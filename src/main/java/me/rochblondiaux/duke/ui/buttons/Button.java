package me.rochblondiaux.duke.ui.buttons;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.graphics.TextRenderer;
import de.gurkenlabs.litiengine.gui.GuiComponent;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public class Button extends GuiComponent {

    private ButtonStyle style;
    private ButtonState state;
    private String text;

    public Button(@NonNull ButtonStyle style, @NonNull String text, double x, double y) {
        super(x, y);
        this.style = style;
        this.state = ButtonState.NORMAL;
        this.text = text;

        BufferedImage image = style.getSpritesheet().getSprite(0);
        this.onHovered(componentMouseEvent -> {
            setState(ButtonState.HOVER);
            Game.loop().perform(50, this::recalculateState);
        });

        this.onClicked(componentMouseEvent -> {
            setState(ButtonState.CLICK);
            Game.loop().perform(50, this::recalculateState);
        });
    }

    @Override
    public void render(Graphics2D g) {
        g.drawImage(style.getSpritesheet().getSprite(state.getSprite()), (int) getX(), (int) getY(), (int) getWidth(), (int) getHeight(), null);

        if (Objects.nonNull(text) && !text.isEmpty()) {
            g.setColor(Color.WHITE);
            g.setFont(getFont());

            TextRenderer.render(g, text, getX() + (getWidth() / 2 - TextRenderer.getWidth(g, text) / 2),
                    getY() + (getHeight() / 2 + TextRenderer.getHeight(g, text) / 4),
                    true);
            return;
        }

        super.render(g);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (!enabled)
            setState(ButtonState.DISABLED);
    }

    private void recalculateState() {
        switch (state) {
            case CLICK:
                if (isPressed()) {
                    Game.loop().perform(50, this::recalculateState);
                    return;
                }
                if (isHovered())
                    setState(ButtonState.HOVER);
                else
                    setState(ButtonState.NORMAL);
                break;
            case HOVER:
                if (isPressed())
                    setState(ButtonState.CLICK);
                if (isHovered()) {
                    Game.loop().perform(50, this::recalculateState);
                    return;
                }
                setState(ButtonState.NORMAL);
                break;
            case NORMAL:
            case DISABLED:
            default:
                break;
        }
    }
}
