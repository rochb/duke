package me.rochblondiaux.duke.ui.buttons;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@RequiredArgsConstructor
@Getter
public enum ButtonState {
    CLICK(1),
    HOVER(2),
    NORMAL(0),
    DISABLED(0);

    private final int sprite;

}
