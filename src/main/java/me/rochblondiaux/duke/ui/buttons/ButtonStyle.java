package me.rochblondiaux.duke.ui.buttons;

import de.gurkenlabs.litiengine.graphics.Spritesheet;
import lombok.Data;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class ButtonStyle {

    private final String name;
    private final Spritesheet spritesheet;

}
