package me.rochblondiaux.duke.ui.buttons;

import de.gurkenlabs.litiengine.graphics.Spritesheet;
import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.resources.ResourcesContainer;
import lombok.NonNull;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class ButtonStyles extends ResourcesContainer<ButtonStyle> {

    @Override
    protected ButtonStyle load(URL url) throws Exception {
        BufferedImage image = Resources.images().get(url, true);
        if (Objects.isNull(image))
            return null;
        Spritesheet spritesheet = new Spritesheet(image, url.getPath(), image.getWidth(), image.getHeight() / 3);
        return new ButtonStyle(getName(url), spritesheet);
    }

    private String getName(@NonNull URL url) {
        return url.getFile().split("-")[3].replace(".png", " ");
    }

}
