package me.rochblondiaux.duke.ui.popup;

import de.gurkenlabs.litiengine.Game;
import de.gurkenlabs.litiengine.gui.ComponentMouseEvent;
import lombok.NonNull;
import me.rochblondiaux.duke.Duke;
import me.rochblondiaux.duke.game.GameManager;
import me.rochblondiaux.duke.game.GameState;
import me.rochblondiaux.duke.ui.buttons.Button;

import java.util.function.Consumer;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PausePopUp extends PopUp {

    public PausePopUp(@NonNull PopUpStyle style, double x, double y, double size) {
        super(style, x, y, size);

        double x1 = x + (getWidth() / 2);
        double y1 = y + (getHeight() / 12);
        double space = this.getHeight() / 4;

        // Resume
        addButton("Resume", x1, y1, componentMouseEvent -> GameManager.setState(GameState.IN_GAME));

        // Options
        y1 += space;
        addButton("Options", x1, y1, componentMouseEvent -> {
            Game.window().getRenderComponent().fadeOut(1000);
            Game.loop().perform(1500, () -> {
                GameManager.setState(GameState.OPTION);
                Game.window().getRenderComponent().fadeIn(1000);
            });
        });

        // Quit
        y1 += space;
        addButton("Quit", x1, y1, componentMouseEvent -> System.exit(0));
    }

    private void addButton(@NonNull String text, double x, double y, @NonNull Consumer<ComponentMouseEvent> clickAction) {
        double width = this.getWidth() / 1.5d;
        double height = this.getHeight() / 6d;
        Button button = new Button(Duke.buttonStyles().get("small"), text, x - width / 2.0, y + height / 2.0);
        button.setDimension(width, height);
        button.setFont(Duke.GUI_FONT.deriveFont(55f));
        button.onClicked(componentMouseEvent -> {
            getComponents().forEach(guiComponent -> guiComponent.setEnabled(false));
            clickAction.accept(componentMouseEvent);
            Game.loop().perform(1500, () -> getComponents().forEach(guiComponent -> guiComponent.setEnabled(true)));
        });
        this.getComponents().add(button);
    }
}
