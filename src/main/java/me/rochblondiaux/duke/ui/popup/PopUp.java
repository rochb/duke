package me.rochblondiaux.duke.ui.popup;

import de.gurkenlabs.litiengine.graphics.ImageRenderer;
import de.gurkenlabs.litiengine.gui.GuiComponent;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.awt.*;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Getter
@Setter
public class PopUp extends GuiComponent {

    private PopUpStyle style;
    private double size;

    public PopUp(@NonNull PopUpStyle style, double x, double y, double size) {
        super(x, y);
        this.style = style;
        this.size = size;
        setDimension(style.getImage().getWidth() * size, style.getImage().getHeight() * size);
    }

    @Override
    public void render(Graphics2D g) {
        ImageRenderer.renderScaled(g, style.getImage(), getLocation(), size);
        super.render(g);
    }
}
