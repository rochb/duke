package me.rochblondiaux.duke.ui.popup;

import lombok.Data;

import java.awt.image.BufferedImage;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
@Data
public class PopUpStyle {

    private final String name;
    private final BufferedImage image;

}
