package me.rochblondiaux.duke.ui.popup;

import de.gurkenlabs.litiengine.resources.Resources;
import de.gurkenlabs.litiengine.resources.ResourcesContainer;

import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Objects;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class PopUpStyles extends ResourcesContainer<PopUpStyle> {

    @Override
    protected PopUpStyle load(URL url) throws Exception {
        BufferedImage image = Resources.images().get(url);
        if (Objects.isNull(image))
            return null;
        return new PopUpStyle(url.getFile().replace(".png", ""), image);
    }


}
