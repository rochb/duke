package me.rochblondiaux.duke.utils;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Roch Blondiaux
 * www.roch-blondiaux.com
 */
public class RandomUtil {

    public static int random(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }
}
